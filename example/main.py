from PackageManager import PackageManager
import logging

logging.basicConfig(level=logging.DEBUG)
pm = PackageManager(origin='example', path='packages')
pm.start()
pm.activate_all()
pm.deactivate_all()
