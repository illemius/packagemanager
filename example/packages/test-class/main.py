from PackageManager.builtins import package, get_manager


class Plugin:
    def on_init(self):
        print('Hello from', package().name)
        package().public(self.test_method)
        package().public(self.activate)
        package().public(self.deactivate)

    def test_method(self):
        print('Test method')

    def event_foobar(self):
        print('->\tCalled', self.event_foobar.__name__, 'from', __package__)

    def activate(self):
        pass

    def deactivate(self):
        pass
