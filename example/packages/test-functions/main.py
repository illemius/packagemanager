from PackageManager.builtins import package, load, get_manager


def on_init():
    print('Hello from', package().name)
    test = load('test_CBS', 'test_method')
    test()
    get_manager().call_event('foobar')


def event_foobar():
    print('->\tCalled', event_foobar.__name__, 'from', __package__)
