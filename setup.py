from distutils.core import setup
from PackageManager import __version__

setup(
    name='PackageManager',
    version=__version__,
    packages=['PackageManager', 'PackageManager.models'],
    url='https://bitbucket.org/illemius/packagemanager/',
    license='MIT',
    author='Illemius/Alex Root Junior',
    author_email='jroot.junior@gmail.com',
    description='Package manager'
)
