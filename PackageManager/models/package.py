import os

from ..utils import get_field

PLUGIN_VAR = 'plugin'
PLUGIN_CLASS = 'Plugin'
INIT_METHOD = 'on_init'


class Package:
    def __init__(self, manager, path, **kwargs):
        """
        Base package info
        :param path:
        :param kwargs:
        """
        self.manager = manager
        self.path = path

        self.origin = get_field(kwargs, 'origin', 'app')
        self.name = get_field(kwargs, 'package', path)
        self.version = get_field(kwargs, 'version', '0')
        self.api = get_field(kwargs, 'api', 2)
        self.author = get_field(kwargs, 'author', '')
        self.description = get_field(kwargs, 'description', '')
        self.url = get_field(kwargs, 'url', '')
        self.license = get_field(kwargs, 'license', '')
        self.license_text_file = get_field(kwargs, 'license_text', '')
        self.tags = get_field(kwargs, 'tags', [])

        setup = get_field(kwargs, 'setup', {})

        init_script = get_field(setup, 'init_script', 'main')
        if init_script.endswith('.py'):
            init_script = init_script[:-3]
        self.init_script = init_script
        self.load_priority = get_field(setup, 'load_priority', 1000)
        self.requirements = get_field(setup, 'requirements', [])

        self.module_name = os.path.basename(self.path)
        self.module_link = '.'.join([self.module_name, self.init_script])
        self.main_script = os.path.join(self.path, self.init_script)

        self.module = None
        self._main_module = None

        self.mode = 'None'
        self._init_method = None

        self.public_methods = {}
        self.activated = None

    @property
    def main_module(self):
        if self.module is not None and self._main_module is None:
            self._main_module = getattr(self.module, self.init_script)
        return self._main_module

    def get_license_text(self):
        if os.path.isfile(os.path.join(self.path, self.license_text_file)):
            with open(self.license_text_file, 'r') as file:
                return '\n'.join(file.readlines())
        return self.license_text_file

    def import_main(self, globals_=None, locals_=None):
        if os.path.isfile(self.main_script + '.py'):
            self.module = __import__(self.module_link, globals_, locals_)
            setattr(self.main_module, '__package_object__', self)
            setattr(self.main_module, '__manager_instance__', self.manager)
            for key, value in globals_:
                setattr(self.main_module, key, value)

    def check_mode(self):
        """
        Check package mode - class based or functions based
        :return:
        """
        if self.main_module is None:
            raise RuntimeError('Main module from "{}" is not loaded!'.format(self.name))

        if hasattr(self.main_module, PLUGIN_CLASS) \
                and hasattr(getattr(self.main_module, PLUGIN_CLASS), INIT_METHOD) \
                and not hasattr(self.main_module, PLUGIN_VAR):
            setattr(self.main_module, PLUGIN_VAR, getattr(self.main_module, PLUGIN_CLASS)())

        if hasattr(self.main_module, PLUGIN_VAR):
            self.mode = PackageMode.CLASS
            self._init_method = getattr(getattr(self.main_module, PLUGIN_VAR), INIT_METHOD)
        elif hasattr(self.main_module, INIT_METHOD):
            self.mode = PackageMode.FUNCTIONS
            self._init_method = getattr(self.main_module, INIT_METHOD)

    def init(self):
        self.check_mode()
        self._init_method()

    def public(self, method, name=None):
        """
        Register public method
        :return:
        """
        self.public_methods.update({name or method.__name__: method})

    def use(self, attr):
        """
        get public method
        :param attr:
        :return:
        """
        if attr in self.public_methods:
            return self.public_methods[attr]
        raise AttributeError("Package '{}' has no public attribute '{}'".format(self.name, attr))

    def use_public(self, attr, *args, **kwargs):
        """
        Call public method
        :param attr:
        :param args:
        :param kwargs:
        :return:
        """
        return self.use(attr)(*args, **kwargs)

    def __str__(self):
        return self.name


class PackageMode:
    FUNCTIONS = 'FBS'
    CLASS = 'CBS'
