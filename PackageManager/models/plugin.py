class PluginBase:
    """
    Skeleton for plugin based on classes
    Not required.
    """
    def on_init(self):
        raise NotImplementedError()
