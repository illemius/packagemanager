def get_field(data, key, default=None):
    if key in data:
        result = data[key]
        del data[key]
        return result
    return default
