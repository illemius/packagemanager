class RequirementsError(KeyError):
    pass


class UnknownPackage(KeyError):
    pass
