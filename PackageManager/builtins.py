import sys

from .loader import PackageManager
from .models import Package


def use(attr):
    """
    get attribute from module
    :param attr:
    :return:
    """
    frame = sys._getframe(1)
    global_vars = frame.f_globals
    if attr in global_vars:
        return global_vars[attr]


def package() -> Package:
    """
    Get package instance from current module
    :return:
    """
    frame = sys._getframe(1)
    global_vars = frame.f_globals
    if '__package_object__' in global_vars:
        return global_vars['__package_object__']
    raise NotImplementedError("'__package_object__' is not implemented in '{}'!".format(frame.f_code.co_name))


def get_manager() -> PackageManager:
    """
    Get PackageManager instance from current module
    :return:
    """
    frame = sys._getframe(1)
    global_vars = frame.f_globals
    if '__manager_instance__' in global_vars:
        return global_vars['__manager_instance__']
    raise NotImplementedError("'__manager_instance__' is not implemented in '{}'!".format(frame.f_code.co_name))


def load(package_name, attr):
    """
    Get attribute from required package
    :param package_name:
    :param attr:
    :return:
    """
    frame = sys._getframe(1)
    global_vars = frame.f_globals
    if '__manager_instance__' not in global_vars:
        raise NotImplementedError("'__manager_instance__' is not implemented in '{}'!".format(frame.f_code.co_name))
    if '__package_object__' not in global_vars:
        raise NotImplementedError("'__package_object__' is not implemented in '{}'!".format(frame.f_code.co_name))
    package_manager = global_vars['__manager_instance__']
    pkg = global_vars['__package_object__']

    assert isinstance(package_manager, PackageManager)
    assert isinstance(pkg, Package)

    loaded_packages = [item.name for item in package_manager.packages]
    try:
        index = loaded_packages.index(package_name)
    except ValueError:
        raise ImportError("Package '{}' is not loaded!".format(package_name))
    else:
        target_pkg = package_manager.packages[index]
        return target_pkg.use(attr)


def package_instance(function):
    """
    Decorator for class methods
    Add plugin instance to context
    :param function:
    :return:
    """
    frame = sys._getframe(1)

    def wrapper(*args, **kwargs):
        global_vars = frame.f_globals
        if 'plugin' in global_vars:
            plugin = global_vars['plugin']
        else:
            raise NotImplementedError("'plugin' is not implemented in '{}'!".format(frame.f_code.co_name))

        if plugin != args[0] or kwargs.get('self', None) != plugin:
            return function(plugin, *args, **kwargs)
        return function(*args, **kwargs)

    return wrapper
