import json
import logging
import os
import sys

from PackageManager.models.package import PackageMode
from .exceptions import RequirementsError, UnknownPackage
from .models import Package

log = logging.getLogger('PackageManager')


class PackageManager:
    """
    Base of package manager
    """

    def __init__(self, path='packages', origin="app", settings_filename='package.json'):
        """
        Init package manager
        :param path:
        :param origin:
        :param settings_filename:
        """
        path = os.path.abspath(path)
        if not os.path.isdir(path):
            raise FileExistsError(path)

        self.path = path
        self.origin = origin
        self.settings_filename = settings_filename
        self.globals = {}
        self.locals = {}

        sys.path.insert(0, self.path)
        self.packages = []

    def _scan_dir(self):
        """
        Scan path for packages
        Is generator
        :return:
        """
        for root, dirs, files in os.walk(self.path):
            for file in files:
                if file == self.settings_filename:
                    yield root

    def _find_packages(self):
        """
        Load packages data and check origin
        Is generator
        :return:
        """
        for root in self._scan_dir():
            with open(os.path.join(root, self.settings_filename), 'r') as file:
                try:
                    pkg_data = json.load(file)
                except json.JSONDecodeError:
                    continue
                else:
                    package_meta = Package(self, root, **pkg_data)
                    if self.check_origin(package_meta.origin):
                        yield package_meta

    def import_packages(self):
        """
        Import packages
        :return:
        """
        for package in sorted(self._find_packages(), key=lambda p: p.load_priority):
            try:
                loaded_packages = [pkg.name for pkg in self.packages]
                for require in package.requirements:
                    if require not in loaded_packages:
                        raise RequirementsError(require)
                package.import_main(self.globals, self.locals)
            except ImportError as e:
                log.error("Wrong package '{}': {}".format(package.name, e))
            else:
                self.packages.append(package)

    def init_packages(self):
        """
        Execute 'on_init' method from packages
        :return:
        """
        for package in self.packages:
            try:
                package.init()
                log.debug("Inited: {}".format(package.name))
            except RuntimeError as e:
                log.error("Error occurred loading '{}': {}".format(package.name, e))
                continue

    def start(self):
        """
        Start application
        :return:
        """
        self.import_packages()
        self.init_packages()
        log.info("Loaded {} packages".format(len(self.packages)))

    def check_origin(self, variant):
        """
        Check app origin
        :param variant:
        :return:
        """
        return variant == self.origin or variant == '*'

    def call_event(self, event, once=True, args=None, kwargs=None):
        """
        Call event to package
        :param event:
        :param once:
        :param args:
        :param kwargs:
        :return:
        """
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}

        event_title = 'event_{}'.format(event)
        results = []
        for package in self.packages:
            if package.mode == PackageMode.FUNCTIONS and hasattr(package.main_module, event_title):
                # FBS mode
                callback = getattr(package.main_module, event_title)
            elif package.mode == PackageMode.CLASS and hasattr(getattr(package.main_module, 'plugin'), event_title):
                # CBS mode
                callback = getattr(getattr(package.main_module, 'plugin'), event_title)
            else:
                continue

            result = callback(*args, **kwargs)
            if once and result:
                return result
            results.append(result)
        return results

    def get_package(self, package_name) -> Package:
        if not isinstance(package_name, Package):
            for package in self.packages:
                if package.name == package_name:
                    return package
            if isinstance(package_name, str):
                raise UnknownPackage(package_name)
        return package_name

    def activate(self, package_name):
        package = self.get_package(package_name)
        if not package.activated:
            package.use_public('activate')
            package.activated = True
            log.info('Package "{}" is activated.'.format(package.name))
            return True
        log.warning('Package "{}" is already activated!'.format(package.name))
        return False

    def activate_all(self, handle_errors=True, error_handler=None):
        for package in self.packages:
            try:
                self.activate(package)
            except AttributeError:
                continue
            except:
                if handle_errors and callable(error_handler):
                    error_handler()
                elif handle_errors and not callable(error_handler):
                    raise
                else:
                    continue

    def deactivate(self, package_name):
        package = self.get_package(package_name)
        if package.activated:
            package.use_public('deactivate')
            package.activated = False
            log.info('Package "{}" is deactivated.'.format(package.name))
            return True
        log.warning('Package "{}" is not activated!'.format(package.name))
        return False

    def deactivate_all(self, handle_errors=True, error_handler=None):
        for package in self.packages:
            if package.activated is None:
                continue
            try:
                self.deactivate(package)
            except AttributeError:
                continue
            except:
                if handle_errors and callable(error_handler):
                    error_handler()
                elif handle_errors and not callable(error_handler):
                    raise
                else:
                    continue
